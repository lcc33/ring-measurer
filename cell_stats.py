import numpy as np
import math
import scipy.stats as stats
import pandas as pd



def fwhm_boundaries(cross_section):
  cross_section = cross_section - np.min(cross_section)
  min_mean = np.mean([cross_section[0:5], cross_section[-6:-1]])
  max_mean = np.mean(np.sort(cross_section)[-4:-1])
  center_ind = int(round(np.mean(np.argsort(cross_section)[-4:-1])))
  mid = (max_mean-min_mean)/2
  distance_from_mid = np.abs(np.subtract(cross_section, mid))
  left_ind = np.argmin(distance_from_mid[0:center_ind])
  right_ind = np.argmin(distance_from_mid[center_ind:-1])+center_ind
  return left_ind, right_ind

def determine_internal_min_max(cross_section):
  section_sort = np.sort(cross_section)
  section_max = np.mean(section_sort[-5:-1])
  section_min = np.mean(section_sort[0:5])
  return section_min, section_max

def compare_variable(results, variable):
  samples = results['sample'].unique()
  same_as_unfixed = [False]*(len(samples))
  same_as_unfixed[0] = True
  sample_1 = results.loc[results['sample'] == samples[0]]
  data_1 = sample_1[variable]
  column_name = variable+'_p_value'
  comparison_result = pd.DataFrame()
  comparison_result['sample'] = samples
  p_value = np.zeros(len(samples))
  if len(sample_1)>1:
    for ii in range(1, len(samples)):
      comparison_result.at[ii, 'sample'] = samples[ii]
      sample_2 = results.loc[results['sample'] == samples[ii]]
      data_2 = sample_2[variable]
      p = welchs_ttest(data_1, data_2)
      p_value[ii] = p
  comparison_result[column_name] = p_value
  return comparison_result

def welchs_ttest(data_1, data_2):
  num = data_1.mean() - data_2.mean()
  den = data_1.var()**2/len(data_1) + data_2.var()**2/len(data_2)
  t = num/math.sqrt(den)
  num = data_1.var()**2/len(data_1) + data_2.var()**2/len(data_2)
  den = data_1.var()**4/(len(data_1)**2*(len(data_1)-1)) + data_2.var()**4/(len(data_2)**2*(len(data_2)-1))
  df = round(num**2/den)
  p = stats.t.pdf(t, df)
  return p
