import numpy as np
import matplotlib.pyplot as plt
from single_cell import SingleCell
from sample_handler import SampleHandler
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import cell_stats
import seaborn as sns
import os

class CellComparison():
  def __init__(self, samples):
    self.samples = samples
    self.extract_results()
    self.plot_example_cells()
    self.compare_variable('ring_height')
    self.compare_variable('width_to_height')
    self.compare_variable('intensity_diff')

  def extract_results(self):
    self.results = pd.DataFrame()
    for ii in range(self.samples.samples_count):
      self.samples.set_active_sample(ii)
      for jj in range(self.samples.files_count):
        file_name = self.samples.files[jj]
        single_cell = SingleCell(file_name)
        self.results = self.results.append(single_cell.results, ignore_index = True)
    self.results_focused = self.results.loc[self.results['focused']==True]

  def plot_example_cells(self):
    plt.rc('figure', figsize=(8, 6))
    pp = PdfPages('results/cell_images.pdf')
    for ii in range(self.samples.samples_count):
      self.samples.set_active_sample(ii)
      cells_to_plot = np.min([self.samples.files_count, 20])
      for jj in range(cells_to_plot):
        file_name = self.samples.files[jj]
        single_cell = SingleCell(file_name)
        plt.clf()
        single_cell.plot_summary()
        pp.savefig()
    pp.close()

  def compare_variable(self, variable):
    comparison = cell_stats.compare_variable(self.results_focused, variable)
    plt.rc('figure', figsize=(8.5, 11))
    file_name = 'compare_' + variable + '.pdf'
    file_name = os.path.join('results', file_name)
    pp = PdfPages(file_name)
    plt.clf()
    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.4)
    sns.swarmplot(x="sample", y=variable, data=self.results, hue='focused')
    plt.xticks(rotation=90)
    plt.draw()
    pp.savefig()
    plt.clf()
    plt.text(0.1, 0.1, comparison.to_string())
    plt.draw()
    pp.savefig()
    pp.close()


    