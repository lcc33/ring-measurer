import os

class SampleHandler():
  def __init__(self, folder_main):
    self.folder_main = folder_main
    self.sample_paths = []
    self.files = []
    self.samples_count = 0
    self.files_count = 0
    self.extract_samples()

  def extract_samples(self):
    for folder in os.walk(self.folder_main):
      if any(folder.endswith('.csv') for folder in os.listdir(folder[0])):
        self.sample_paths.append(folder[0])
    self.samples_count = len(self.sample_paths)

  def extract_files(self):
    self.files = [os.path.join(self.sample_paths[self.active_sample], f) for f in os.listdir(self.sample_paths[self.active_sample])]
    self.files_count = len(self.files)

  def print_summary(self):
    print('sample_paths: ', self.sample_paths)
    print('sample_count: ', self.samples_count)
    print('files_sample: ', self.files)
    print('file_count: ', self.files_count)
  
  def set_active_sample(self, active_sample):
    self.active_sample = active_sample
    self.extract_files()

    