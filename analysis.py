import numpy as np
import matplotlib.pyplot as plt
from single_cell import SingleCell
from sample_handler import SampleHandler
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import cell_stats
import seaborn as sns

from cell_comparison import CellComparison

folder_main = 'data/6-17-19'

results_all = pd.DataFrame()

samples = SampleHandler(folder_main)

cell_comparison = CellComparison(samples)


  