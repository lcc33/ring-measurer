import numpy as np 
import matplotlib.pyplot as plt
from scipy import ndimage
from scipy import signal as sig
from skimage import feature
import csv
import math
import os
from os import listdir
from os.path import isfile, join
from tkinter import filedialog
from tkinter import *
from matplotlib.widgets import Button
import ring_analysis_lib as lib
import sys
import signal

class cell_finder():
  def __init__(self):
    self.reset()
  
  def reset(self):
    self.crop_edit_mode = False
    self.center_edit_mode = False
    self.sample_name = ""
    self.open_file()
    #self.file_name = "/home/lauren/Documents/research/light-microscopy/5-21-19/fomaldehyde_5min/0min/SNAP-150539-0001.tif"
    plt.close('all')
    self.import_image(self.file_name)
    self.cell_centers = []
    self.listen_for_orientation = False
    self.listen_for_center = False
    self.coordinates = []
    self.angle = 0
    self.center = [0, 0]
    self.magnitude = 1
    self.plot_full_image()

  def import_image(self, file_name):
    if (file_name[-1] == 'v'):
      im = np.loadtxt(open(file_name, "rb"), delimiter=",", skiprows=0)
    else:
      im = plt.imread(file_name)      
    im = im[10:,]
    self.im_mean = np.mean(im)
    self.im = np.pad(im, 100, 'mean')

  def plot_full_image(self):
    fig = plt.figure(0)
    fig.clf()
    plt.imshow(self.im)
    for ii in range(len(self.cell_centers)):
      plt.scatter(self.cell_centers[ii][1], self.cell_centers[ii][0], color='red', s=1)
    cid = fig.canvas.mpl_connect('button_press_event', self.pick_cells)
    cid = fig.canvas.mpl_connect('key_press_event', self.begin_cell_analysis)
    plt.show()

  def pick_cells(self, event):
    location = [event.ydata, event.xdata]
    close_points = 0
    remove_points = []
    for ii in range(len(self.cell_centers)):
      center = self.cell_centers[ii]
      distance_between = math.sqrt((center[1]-location[1])**2 + (center[0]-location[0])**2)
      if (distance_between < 20):
        close_points += 1
        remove_points.append(ii)
    if (close_points<1):
      ycoor = int(location[0])
      xcoor = int(location[1])
      self.cell_centers.append([ycoor, xcoor])
      plt.scatter(self.cell_centers[-1][1], self.cell_centers[-1][0], color='red', s=1)
      plt.show()
    for ii in range(len(remove_points)):
      self.cell_centers.pop(remove_points[ii])
      self.plot_full_image()
  
  def plot_single_cell(self):
    fig = plt.figure(self.active_cell)
    plt.clf()
    ax = plt.axes((0.05, 0.05, 0.2, 0.05))  
    button_export = Button(ax, "Export")
    button_export.on_clicked(self.export)
    self.ax = plt.axes((0.05, 0.2, 0.4, 0.75))
    self.ax.imshow(self.single_cell)
    height = 50
    width = 30
    center_y = self.center[0]
    center_x = self.center[1]
    self.ax.set_ylim([center_y-height, center_y+height])
    self.ax.set_xlim([center_x-width, center_x+width])
    plt.axvline(x=center_x, linestyle='dashed')
    plt.axhline(y=center_y, linestyle='dashed')
    ax = plt.axes((0.55, 0.2, 0.4, 0.75))      
    plt.cla()
    lib.plot_cross_sections(self.single_cell)
    cid = fig.canvas.mpl_connect('button_press_event', self.click_listener)
    cid = fig.canvas.mpl_connect('key_press_event', self.adjust_cell)
    plt.show()

  def open_file(self):
    root = Tk()
    root.withdraw()
    fileName = filedialog.askopenfilenames(initialdir='../', title = "Select file", filetypes = (("all files","*.*"),("tiff files","*.tif")))
    path = os.path.normpath(fileName[0])
    path = path.split(os.sep)
    self.sample_name = os.path.join(path[-3], path[-2])
    self.data_path = os.path.join('data', self.sample_name)
    if not os.path.exists(self.data_path):
      os.makedirs(self.data_path)
    self.file_name = fileName[0]
    print(self.file_name)
    root.destroy()

  def export(self, event):
    path = self.file_name.split(os.sep)
    file_name = path[-1][:-4] + '_cell_'+ str(self.active_cell+1) + '.csv'
    file_name = os.path.join(self.data_path, file_name)
    height = 50
    width = 30
    center_y = self.center[0]
    center_x = self.center[1]
    cropped_cell = self.single_cell[center_y-height:center_y+height,center_x-width:center_x+width]
    np.savetxt(file_name, cropped_cell, delimiter=',', fmt='%5.0u')
    plt.close()

  def adjust_cell(self, event):
    if (event.key=='left'):
      self.center[1] += 1*self.magnitude
    elif (event.key=='right'):
      self.center[1] += -1*self.magnitude
    elif (event.key=='up'):
      self.center[0] += -1*self.magnitude
    elif (event.key=='down'):
      self.center[0] += 1*self.magnitude
    elif (event.key=='a'):
      self.angle = -2*self.magnitude
      self.crop_rotate_cell()
    elif (event.key=='d'):
      self.angle = 2*self.magnitude
      self.crop_rotate_cell()
    elif (event.key=='shift'):
      if (self.magnitude == 1):
        self.magnitude = 10
      else:
        self.magnitude = 1
    self.angle = 0
    self.plot_single_cell()

  def click_listener(self, event):
    if (event.ydata>0) and (event.inaxes==self.ax):
      self.coordinates.append([int(event.ydata), int(event.xdata)])
      plt.scatter(self.coordinates[-1][1], self.coordinates[-1][0], color='red', s=1)
      plt.show()

  def begin_cell_analysis(self, event):
    if (event.key == 'enter'):
      for self.active_cell in range(len(self.cell_centers)):
        plt.close()
        self.angle = lib.determine_angle(self.cell_centers[self.active_cell], self.im)
        self.crop_rotate_cell_initial()
        self.plot_single_cell()
      self.reset()
  
  def crop_rotate_cell(self):
    height = 100
    width = 100
    cell = self.single_cell[self.center[0]-height:self.center[0]+height, self.center[1]-width:self.center[1]+width]
    cell = np.pad(cell, 20, 'mean')
    self.single_cell = ndimage.rotate(cell, self.angle)
    self.single_cell[self.single_cell<1] = self.im_mean
    center_y = int(self.single_cell.shape[0]/2)
    center_x = int(self.single_cell.shape[1]/2)
    self.center = [center_y, center_x]
    self.angle = 0
  
  def crop_rotate_cell_initial(self):
    center = self.cell_centers[self.active_cell]
    height = 100
    width = 100
    cell = self.im[center[0]-height:center[0]+height, center[1]-width:center[1]+width]
    self.single_cell = ndimage.rotate(cell, self.angle)
    self.single_cell[self.single_cell<1] = self.im_mean
    center_y = int(self.single_cell.shape[0]/2)
    center_x = int(self.single_cell.shape[1]/2)
    self.center = [center_y, center_x]
    self.angle = 0
  
plot = cell_finder()
